package com.toymobi.single_book_app;

import android.os.Bundle;
import android.view.WindowManager;

import com.toymobi.book.BookFrag;
import com.toymobi.recursos.BaseFragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class BookActivitySingleApp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(com.toymobi.book.R.layout.main_layout_book);
        startBookFrag();
    }

    private void startBookFrag() {

        BookFrag.startSingleApp = true;

        final FragmentManager fragmentManager = getSupportFragmentManager();

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        final BaseFragment fragmentBook = new BookFrag();

        fragmentTransaction.replace(com.toymobi.book.R.id.main_layout_fragment_id,
                fragmentBook, BookFrag.FRAGMENT_TAG_BOOK_TEXT);

        fragmentTransaction.commit();

    }
}