package com.toymobi.book;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.toymobi.framework.adapter.ReducedCustomPagerAdapter;
import com.toymobi.framework.debug.DebugUtility;
import com.toymobi.framework.dimensions.DeviceDimensionsUtil;
import com.toymobi.framework.image.UIComponentUtils;
import com.toymobi.framework.media.MediaPlayerControl;
import com.toymobi.framework.media.MusicPlayer;
import com.toymobi.framework.options.GlobalSettings;
import com.toymobi.framework.persistence.PersistenceManager;
import com.toymobi.framework.raw.GetTextRawFile;
import com.toymobi.framework.view.textview.JustifyTextViewCustomFont;
import com.toymobi.recursos.EduardoStuff;

import androidx.collection.SparseArrayCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.viewpager.widget.ViewPager;

import static com.toymobi.book.BookResources.PAGE_WITHOUT_NARRATION;
import static com.toymobi.book.BookResources.audiosPath;

class BookController {

    private ViewPager viewPager;

    private ReducedCustomPagerAdapter adapter;

    int pageNumber;

    private CharSequence pageNumberText;

    private Context context;

    private SparseArrayCompat<CharSequence> pageText = null;

    private SparseArrayCompat<Drawable> pageImages = null;

    private MediaPlayerControl pageNarration;

    MenuItem menuItemPageNumber;

    private static final String PERSISTENCE_KEY_PAGE = "PERSISTENCE_KEY_PAGE";

    private LayoutInflater layoutInflater;

    BookController(final Context context) {
        DebugUtility.printDebug("BookController Construtor");
        if (context != null) {

            this.layoutInflater = LayoutInflater.from(context);

            this.context = context;

            start();
        }
    }

    private void createAdapter(final LayoutInflater layoutInflater) {

        if (context != null && adapter == null && layoutInflater != null) {

            final int size = BookResources.LAYOUTS_LENGTH;

            adapter = new ReducedCustomPagerAdapter(size);

            for (int i = 0; i < size; i++) {

                View temp = layoutInflater.inflate(BookResources.BOOK_PAGES_LAYOUT[i], null);

                adapter.addViewPage(i, temp);
            }
        }
    }

    private void createViewPager() {
        if (adapter != null) {
            viewPager.setAdapter(adapter);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(final int page) {
                    pageNumber = page;

                    pageNumberText = "" + (pageNumber + 1);

                    if (menuItemPageNumber != null) {
                        menuItemPageNumber.setTitle(pageNumberText);
                    }
                    playNarration();
                }

                @Override
                public void onPageScrolled(final int page,
                                           final float positionOffset,
                                           final int positionOffsetPixels) {
                }

                @Override
                public void onPageScrollStateChanged(final int position) {
                }
            });
        }
    }

    final void startBookText(final View view) {
        if (view != null) {
            viewPager = view.findViewById(R.id.viewPagerBook);
        }
        createViewPager();

        goBookPage(pageNumber);
    }

    private void goBookPage(final int page) {
        if (viewPager != null && pageNumber < BookResources.LAYOUTS_LENGTH
                && page < BookResources.LAYOUTS_LENGTH) {
            pageNumber = page;
            viewPager.setCurrentItem(pageNumber);
            playNarration();
        }
    }

    final void bookPagePreviews() {
        if (viewPager != null) {
            if (pageNumber > 0) {
                --pageNumber;
                viewPager.setCurrentItem(pageNumber);
                playNarration();
            } else {
                pageNumber = BookResources.BOOK_PAGES_SIZE - 1;
                goBookPage(pageNumber);
            }
        }
    }

    final void bookPageNext() {
        if (viewPager != null) {
            if (pageNumber < BookResources.LAYOUTS_LENGTH - 1) {
                ++pageNumber;
                viewPager.setCurrentItem(pageNumber);
                playNarration();
            } else {
                pageNumber = 0;
                goBookPage(pageNumber);
            }
        }
    }

    final void pauseNarration() {
        if (pageNarration != null && GlobalSettings.soundEnable && pageNarration.isPlaying()) {
            pageNarration.pause();
        }
    }

    final void resumeNarration() {
        if (GlobalSettings.soundEnable) {

            if (pageNumber >= 0 && pageNumber < audiosPath.length) {
                if (BookResources.audiosPath[pageNumber] != PAGE_WITHOUT_NARRATION) {
                    pageNarration.play();
                }
            }
        }
    }

    final void playNarration() {

        if (GlobalSettings.soundEnable) {

            if (pageNumber >= 0 && pageNumber < audiosPath.length) {

                stopNarration();

                if (BookResources.audiosPath[pageNumber] != PAGE_WITHOUT_NARRATION) {

                    pageNarration = createLocution(
                            BookResources.audiosPath[pageNumber],
                            BookResources.audiosRawIdPath[pageNumber]);

                    pageNarration.loadMedia(true);
                }
            }
        }

        if (DebugUtility.IS_DEBUG) {
            DebugUtility.printDebug("playNarration");
        }
    }

    final void stopNarration() {
        if (pageNarration != null) {
            pageNarration.release();
        }
    }

    final void releaseNarration() {
        if (pageNarration != null) {
            pageNarration.release();
            pageNarration = null;
        }
    }

    private void createPageImage() {

        if (context != null) {

            final int size = BookResources.BOOK_PAGES_IMAGE.length;

            if (pageImages == null) {

                pageImages = new SparseArrayCompat<>(size);

                final Resources resources = context.getResources();

                if (resources != null) {
                    for (int i = 0; i < size; i++) {

                        final int pageIndexID = BookResources.BOOK_PAGES_IMAGE[i];

                        if (pageIndexID != -1) {

                            final Drawable imageDrawable = ResourcesCompat.getDrawable(resources, pageIndexID, null);

                            if (imageDrawable != null) {
                                pageImages.put(i, imageDrawable);
                            } else {
                                pageImages.put(i, null);
                            }

                        } else {
                            pageImages.put(i, null);
                        }
                    }
                }
            }
        }
    }

    private void createPageImageLow() {

        if (context != null) {

            final int size = BookResources.BOOK_PAGES_IMAGE.length;

            final int height = DeviceDimensionsUtil.getDisplayHeight(context) / 2;

            final int width = height + (height / 4);

            if (pageImages == null) {

                pageImages = new SparseArrayCompat<>(size);

                final Resources resources = context.getResources();

                if (resources != null) {

                    BitmapDrawable bkg_image_temp;

                    for (int i = 0; i < size; i++) {

                        final int pageIndexID = BookResources.BOOK_PAGES_IMAGE[i];

                        if (pageIndexID != -1) {

                            bkg_image_temp = new BitmapDrawable(resources,
                                    UIComponentUtils
                                            .decodeSampledBitmapFromResource(
                                                    resources, pageIndexID,
                                                    width, height));

                            pageImages.put(i, bkg_image_temp);

                        } else {
                            pageImages.put(i, null);
                        }
                    }
                }
            }
        }
    }

    private void createPageText() {
        if (pageText == null) {

            final int pageLength = BookResources.BOOK_PAGES_TEXT.length;

            pageText = new SparseArrayCompat<>(pageLength);

            final int size = BookResources.BOOK_PAGES_TEXT.length;

            for (int i = 0; i < size; i++) {

                final int pageIndexID = BookResources.BOOK_PAGES_TEXT[i];

                final CharSequence page = GetTextRawFile.getTextRawFile(
                        context.getResources(), pageIndexID);

                if (page != null) {
                    pageText.put(i, page);
                }
            }
        }
    }

    private void loadPageText() {
        if (adapter != null && pageText != null && pageText.size() > 0) {

            for (int index = 0; index < BookResources.BOOK_PAGES_LAYOUT.length; index++) {

                final View viewTemp = adapter.getViewPage(index);

                if (viewTemp != null) {

                    final JustifyTextViewCustomFont textViewPageText = viewTemp.findViewById(R.id.book_page_layout_text);

                    if (textViewPageText != null && index < pageText.size()) {

                        final CharSequence text = pageText.get(index);

                        if (text != null && text.length() > 0) {
                            textViewPageText.setTextJustify(text);
                        }
                    }
                }
            }
        }
    }

    private void loadPageImage() {

        if (adapter != null && pageImages != null && pageImages.size() > 0) {

            for (int index = 0; index < BookResources.BOOK_PAGES_LAYOUT.length; index++) {

                final View viewTemp = adapter.getViewPage(index);

                if (viewTemp != null) {

                    final ImageView image = viewTemp.findViewById(R.id.book_page_layout_image);

                    if (image != null) {

                        final int pageImageSize = pageImages.size();

                        if (pageImageSize > index) {
                            final Drawable imageDrawable = pageImages
                                    .get(index);

                            if (imageDrawable != null) {
                                image.setImageDrawable(imageDrawable);
                            }
                        }
                    }
                }
            }
        }
    }

    private void start() {

        createAdapter(layoutInflater);

        if (EduardoStuff.isNormalSize(context)) {
            createPageImage();
        } else {
            createPageImageLow();
        }

        createPageText();

        loadPageImage();

        loadPageText();
    }

    final void changePageNumberText() {
        if (menuItemPageNumber != null) {
            if (pageNumber == BookResources.BOOK_PAGES_LAYOUT.length) {
                pageNumberText = "" + BookResources.BOOK_PAGES_LAYOUT.length;
            } else {
                pageNumberText = "" + (pageNumber + 1);
            }

            menuItemPageNumber.setTitle(pageNumberText);
        }
    }

    final void clearAll() {

        if (pageText != null && pageText.size() > 0) {
            pageText.clear();
        }

        if (adapter != null) {
            for (int index = 0; index < BookResources.BOOK_PAGES_LAYOUT.length; index++) {

                final View viewTemp = adapter.getViewPage(index);

                if (viewTemp != null) {

                    ImageView picture = viewTemp.findViewById(R.id.book_page_layout_image);

                    if (picture != null) {
                        picture.setImageDrawable(null);
                        picture.setImageBitmap(null);
                        //picture = null;
                    }
                }
            }
            adapter.deallocate();
            adapter = null;
        }

        if (viewPager != null) {
            viewPager.removeAllViews();
            viewPager.removeAllViewsInLayout();
            viewPager = null;
        }

        if (pageImages != null) {
            pageImages.clear();
            pageImages = null;

            layoutInflater = null;
        }
    }

    final void savePage() {
        if (EduardoStuff.ENABLE_SAVE_PAGE_BOOK) {
            PersistenceManager.saveSharedPreferencesInt(context,
                    PERSISTENCE_KEY_PAGE, pageNumber);
        }
    }

    final void loadPage() {

        if (EduardoStuff.ENABLE_SAVE_PAGE_BOOK) {
            pageNumber = PersistenceManager.loadSharedPreferencesInt(context,
                    PERSISTENCE_KEY_PAGE, 0);
        }
    }

    private MediaPlayerControl createLocution(final int musicNameId, final int musicRawID) {

        MediaPlayerControl musicPlayer = null;

        DebugUtility.printDebug("createLocution");

        if (context != null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                musicPlayer = new MusicPlayer(context, musicRawID, false);

            } else {

                final String packageName = context.getPackageName();

                final String musicPath = context.getString(musicNameId);

                final String music = GetTextRawFile.getRawSoundsFileFixedPathPackage(packageName, musicPath);

                musicPlayer = new MusicPlayer(context, music, false);
            }
        }
        return musicPlayer;
    }
}
