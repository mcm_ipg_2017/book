package com.toymobi.book;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.toymobi.framework.options.GlobalSettings;
import com.toymobi.recursos.BaseFragment;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

public class BookFrag extends BaseFragment {

    private BookController bookController;

    public static final String FRAGMENT_TAG_BOOK_TEXT = "FRAGMENT_TAG_BOOK_TEXT";

    @Override
    public void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        createMusicBackground(R.string.path_sound_book, R.raw.book_sound, true);

        createSFX(R.raw.sfx_normal_click);

        createVibrationeedback();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (bookController != null) {
            bookController.loadPage();
            bookController.resumeNarration();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (bookController != null) {
            bookController.pauseNarration();
            bookController.savePage();
        }
    }

    @Override
    public final void onCreateOptionsMenu(@NonNull final Menu menu,
                                          @NonNull final MenuInflater inflater) {

        menu.clear();

        inflater.inflate(R.menu.menu_book_text, menu);

        // Set an icon in the ActionBar
        itemSound = menu.findItem(R.id.sound_menu_book);

        setIconSoundMenu();

        final MenuItem itemPageNumber = menu.findItem(R.id.number_page_menu_book);

        if (itemPageNumber != null) {

            if (bookController != null) {
                bookController.menuItemPageNumber = itemPageNumber;
                bookController.changePageNumberText();
            }
        }
    }

    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   final ViewGroup container,
                                   final Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.book_fragment, container, false);

        if (bookController == null)
            bookController = new BookController(getActivity());

        createToolBar(R.id.toolbar_book, R.string.book_name_toolbar);

        if (mainView != null) {
            bookController.startBookText(mainView);
        }
        return mainView;
    }

    @Override
    public final boolean onOptionsItemSelected(@NonNull final MenuItem item) {

        final int item_id = item.getItemId();

        playFeedBackButtons();

        if (item_id == R.id.back_menu_book) {

            exitOrBackMenu(startSingleApp);

        } else if (item_id == R.id.sound_menu_book) {

            changeSoundMenu();

            if (GlobalSettings.soundEnable) {
                bookController.playNarration();
            } else {
                bookController.stopNarration();
            }

        } else if (item_id == R.id.preview_page_menu_book) {

            if (bookController.pageNumber != 0) {
                bookController.bookPagePreviews();
            }

        } else if (item_id == R.id.next_page_menu_book) {

            if (bookController.pageNumber != BookResources.LAYOUTS_LENGTH - 1) {
                bookController.bookPageNext();
            }

        } else {
            super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        deallocate();
    }

    @Override
    public void deallocate() {

        super.deallocate();

        if (mainView != null) {
            ((ConstraintLayout) mainView).removeAllViews();
        }

        if (bookController != null) {
            bookController.savePage();
            bookController.releaseNarration();
            bookController.clearAll();
            bookController = null;
        }
    }
}
