package com.toymobi.book;

interface BookResources {

    int[] BOOK_PAGES_TEXT = {R.raw.book_page_01,
            R.raw.book_page_02, R.raw.book_page_03, R.raw.book_page_04,
            R.raw.book_page_05, R.raw.book_page_06, R.raw.book_page_07,
            BookResources.PAGE_WITHOUT_NARRATION};

    int BOOK_PAGES_SIZE = BOOK_PAGES_TEXT.length;

    int[] BOOK_PAGES_IMAGE = {R.drawable.page_1,
            R.drawable.page_2, R.drawable.page_3, R.drawable.page_4,
            R.drawable.page_5, R.drawable.page_6, R.drawable.page_7,
            R.drawable.page_8};

    int[] BOOK_PAGES_LAYOUT = {R.layout.book_card,
            R.layout.book_card, R.layout.book_card,
            R.layout.book_card, R.layout.book_card,
            R.layout.book_card, R.layout.book_card,
            R.layout.book_simple_end};

    int PAGE_WITHOUT_NARRATION = -1;

    int[] audiosPath = {R.string.narration_page_1,
            R.string.narration_page_2, R.string.narration_page_3,
            R.string.narration_page_4, R.string.narration_page_5,
            R.string.narration_page_6, R.string.narration_page_7,
            BookResources.PAGE_WITHOUT_NARRATION};

    int[] audiosRawIdPath = {
            R.raw.book_1,
            R.raw.book_2,
            R.raw.book_3,
            R.raw.book_4,
            R.raw.book_5,
            R.raw.book_6,
            R.raw.book_7,
            BookResources.PAGE_WITHOUT_NARRATION};

    int LAYOUTS_LENGTH = BOOK_PAGES_LAYOUT.length;
}
